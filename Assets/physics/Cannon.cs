﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Cannon : MonoBehaviour
{
    // in Unity you don't create new gameobjects from scratch
    // we clone! (instantiate)
    public GameObject original;
    public Transform reference;
    public float fuerzaBala = 10;

    private IEnumerator corrutinaEnumerator;
    private Coroutine corrutina;

    // propiedad (property)

    public static Cannon Instancita {
        get;
        private set;
    }

    // Start is called before the first frame update
    void Start()
    {
        Instancita = this;

        // para código concurrente
        // - async
        // - corrutinas
        
        //StartCoroutine("EjemploDeCorrutina");
    }

    // Update is called once per frame
    void Update()
    {
        float h = Input.GetAxis("Horizontal");
        transform.Translate(0, 0, h * Time.deltaTime * 10);

        if (Input.GetKeyDown(KeyCode.Space)) {

            // rotación definida por un cuaternión

            // crear cuaternión manualmente
            Quaternion ejemplo = Quaternion.Euler(50, 0, 0);

            corrutinaEnumerator = EjemploDeCorrutina();
            corrutina = StartCoroutine(corrutinaEnumerator);

        }

        if (Input.GetKeyUp(KeyCode.Space)) {

            //StopAllCoroutines();
            //StopCoroutine("EjemploDeCorrutina");
            //StopCoroutine(corrutinaEnumerator);
            StopCoroutine(corrutina);
        }

    }

    // corrutina se define en un método
    IEnumerator EjemploDeCorrutina() {

        while (true) {
            Instantiate(original, reference.position, reference.rotation);
            yield return new WaitForSeconds(0.5f);
        }        
    }
}
