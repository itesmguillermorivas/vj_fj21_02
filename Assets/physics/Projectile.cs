﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

//[RequireComponent(typeof(Rigidbody))]
public class Projectile : MonoBehaviour
{

    Rigidbody rb;

    // Start is called before the first frame update
    void Start()
    {

        // retrieve a reference to another component
        rb = GetComponent<Rigidbody>();

        // 3 reference vectors
        // transform.up
        // transform.right
        // transform.forward
        // all of them unit vectors

        // como buscar objetos en escena programáticamente

        /*
        GameObject cannon = GameObject.Find("Canion");
        Cannon c = cannon.GetComponent<Cannon>();
        */

        //Cannon c = GameObject.FindObjectOfType<Cannon>();



        rb.AddForce(transform.up * Cannon.Instancita.fuerzaBala, ForceMode.Impulse);

        // GameObject.Find("");
        Destroy(gameObject, 5);
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    private void OnCollisionEnter(Collision collision)
    {
        /*
        print(collision.transform.tag);
        print(collision.gameObject.layer);
        */

        if (collision.transform.tag == "ladrillo") {

            // destrucción
            Destroy(collision.gameObject);

            //Destroy(rb);
        }
    }
}
