﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[RequireComponent(typeof(CharacterController))]
public class Character : MonoBehaviour
{

    public float speed;

    private CharacterController characterController;

    // Start is called before the first frame update
    void Start()
    {
        characterController = GetComponent<CharacterController>();
        //StartCoroutine(DispararRayo());

    }

    // Update is called once per frame
    void Update()
    {

        float horizontalAxis = Input.GetAxis("Horizontal");
        float verticalAxis = Input.GetAxis("Vertical");

        // 1st easier way to move a character with a richer behaviour
        characterController.SimpleMove(new Vector3(-verticalAxis * speed, 0, horizontalAxis * speed));
        //characterController.Move(new Vector3(-verticalAxis, 0, horizontalAxis) * speed * Time.deltaTime);

        

    }

    IEnumerator DispararRayo() {

        RaycastHit hit;

        while (true) {


            if (Physics.Raycast(transform.position, -transform.right, out hit))
            {
                print("CHOQUE CON " + hit.transform.name);
            }
            else
            {
                print("NADA!");
            }

            yield return new WaitForSeconds(0.5f);
        }
    }

    // 2nd collision detection
    private void OnControllerColliderHit(ControllerColliderHit hit)
    {
        //print(hit.transform.name + ", " + hit.transform.tag + ", " + hit.gameObject.layer);
    }

    private void OnDrawGizmos()
    {
        Gizmos.color = Color.red;
        Gizmos.DrawRay(transform.position, -transform.right);
    }


    private void OnMouseEnter()
    {
        //print("ENTER");
    }

    private void OnMouseOver()
    {
        //print("OVER");
    }

    private void OnMouseExit()
    {
        //print("EXIT");
    }
        

    private void OnMouseDown()
    {
        print("DOWN");
    }

    private void OnMouseDrag()
    {
        //print("DRAG");
    }

    private void OnMouseUp()
    {
        print("UP");
    }

    private void OnMouseUpAsButton()
    {
        print("UP AS BUTTON");
    }

}
