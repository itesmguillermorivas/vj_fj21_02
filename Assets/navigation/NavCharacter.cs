using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.AI;

public class NavCharacter : MonoBehaviour
{

    private NavMeshAgent agent;


    // Start is called before the first frame update
    void Start()
    {

        agent = GetComponent<NavMeshAgent>();
        //agent.destination = Vector3.zero;
    }

    // Update is called once per frame
    void Update()
    {
        if (Input.GetMouseButtonDown(0)) {

            Ray ray = Camera.main.ScreenPointToRay(Input.mousePosition);
            RaycastHit hit;
            if (Physics.Raycast(ray, out hit)) {

                agent.destination = hit.point;
            }

        }
    }

    // detect motion
    private void OnMouseEnter()
    {
        //print("ENTER");
    }

    private void OnMouseOver()
    {
        //print("OVER");
    }

    private void OnMouseExit()
    {
        //print("EXIT");
    }

    

    // detect click
    private void OnMouseDown()
    {
        print("DOWN");
    }

    private void OnMouseDrag()
    {
        print("DRAG");
    }    

    private void OnMouseUp()
    {
        print("UP");
    }

    private void OnMouseUpAsButton()
    {
        print("UP AS BUTTON");
    }
}
