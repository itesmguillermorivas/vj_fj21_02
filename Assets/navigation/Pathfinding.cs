using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Pathfinding 
{

    public static List<NavNode> Breadthwise(NavNode start, NavNode end)
    {

        // work structures
        Queue<NavNode> work = new Queue<NavNode>();
        List<NavNode> visited = new List<NavNode>();
        // starting node has no history
        start.history = new List<NavNode>();

        // add root node to structures
        work.Enqueue(start);
        visited.Add(start);

        // DO THE LOOP!
        while (work.Count > 0) {

            // get next node
            NavNode current = work.Dequeue();

            // 2 options - the node is the end or not
            if (current == end)
            {
                Debug.Log("FOUND THE END!");
                List<NavNode> result = current.history;
                result.Add(current);
                return result;
            }
            else
            {
                // PROCESS NEIGHBORS
                foreach (NavNode currentNeighbor in current.neighbors) {

                    // check if visited
                    if (!visited.Contains(currentNeighbor)) {

                        work.Enqueue(currentNeighbor);
                        visited.Add(currentNeighbor);

                        // update node history
                        currentNeighbor.history = new List<NavNode>(current.history);
                        currentNeighbor.history.Add(current);

                    }
                }
            }
        }

        // if we get here, there was no result
        Debug.Log("THERE WAS NO PATH :(");
        return null;
    }

    public static List<NavNode> Depthwise(NavNode start, NavNode end)
    {
        // work structures
        Stack<NavNode> work = new Stack<NavNode>();
        List<NavNode> visited = new List<NavNode>();
        // starting node has no history
        start.history = new List<NavNode>();

        // add root node to structures
        work.Push(start);
        visited.Add(start);

        // DO THE LOOP!
        while (work.Count > 0)
        {

            // get next node
            NavNode current = work.Pop();

            // 2 options - the node is the end or not
            if (current == end)
            {
                Debug.Log("FOUND THE END!");
                List<NavNode> result = current.history;
                result.Add(current);
                return result;
            }
            else
            {
                // PROCESS NEIGHBORS
                foreach (NavNode currentNeighbor in current.neighbors)
                {

                    // check if visited
                    if (!visited.Contains(currentNeighbor))
                    {

                        work.Push(currentNeighbor);
                        visited.Add(currentNeighbor);

                        // update node history
                        currentNeighbor.history = new List<NavNode>(current.history);
                        currentNeighbor.history.Add(current);

                    }
                }
            }
        }

        // if we get here, there was no result
        Debug.Log("THERE WAS NO PATH :(");
        return null;
    }

    public static List<NavNode> AStar(NavNode start, NavNode end)
    {

        // work structures
        List<NavNode> work = new List<NavNode>();
        List<NavNode> visited = new List<NavNode>();

        // process root node
        start.history = new List<NavNode>();
        start.g = 0;
        start.h = Vector3.Distance(start.transform.position, end.transform.position);

        // add root to structures
        work.Add(start);
        visited.Add(start);

        // possible case - start is end
        if (start == end)
        {
            List<NavNode> result = new List<NavNode>();
            result.Add(start);
            return result;
        }            

        // the loop
        while (work.Count > 0) {

            // select the next one
            NavNode current = work[0];

            // compare F in all of them
            for (int i = 1; i < work.Count; i++) {

                if (work[i].F < current.F) {

                    current = work[i];
                }
            }

            // remember to remove!
            work.Remove(current);

            // process neighbors
            foreach (NavNode currentNeighbor in current.neighbors) {

                // check if neighbors haven't been visited
                if (!visited.Contains(currentNeighbor)) {

                    // update history
                    currentNeighbor.history = new List<NavNode>(current.history);
                    currentNeighbor.history.Add(current);

                    // NEW! - check if it's the last one
                    if (currentNeighbor == end) {

                        List<NavNode> result = currentNeighbor.history;
                        result.Add(currentNeighbor);
                        return result;
                    }

                    // add to structures
                    work.Add(currentNeighbor);
                    visited.Add(currentNeighbor);

                    // update values for g and h
                    currentNeighbor.g = current.g + Vector3.Distance(current.transform.position, currentNeighbor.transform.position);
                    currentNeighbor.h = Vector3.Distance(currentNeighbor.transform.position, end.transform.position);
                }
            }
        }

        // if there is no answer return null
        return null;
    }
}
