﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class NavNode : MonoBehaviour
{

    public NavNode[] neighbors;
    public List<NavNode> history;
    public float g, h;

    public float F {
        get {
            return g + h;
        }
    }

    // propiedad con variable explícitica
    private float test1;

    public float Test1 {
        get {
            return test1;
        }
        private set {
            test1 = value;
        }
    }

    // con variable anónima
    public float Test2 {
        get;
        private set;
    }

    private void OnDrawGizmos()
    {
        Gizmos.color = Color.blue;
        Gizmos.DrawSphere(transform.position, 1);

        Gizmos.color = Color.yellow;

        foreach (NavNode current in neighbors) {

            if(current != null)
                Gizmos.DrawLine(transform.position, current.transform.position);
        }

    }

    private void OnDrawGizmosSelected()
    {
        Gizmos.color = Color.red;
        Gizmos.DrawSphere(transform.position, 1);


        Gizmos.color = Color.cyan;

        foreach (NavNode current in neighbors)
        {

            if (current != null)
                Gizmos.DrawLine(transform.position, current.transform.position);
        }
    }
}
