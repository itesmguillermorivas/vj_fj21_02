﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class RayCaster : MonoBehaviour
{

    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        if (Input.GetMouseButtonDown(0)) {

            Ray rayito = Camera.main.ScreenPointToRay(Input.mousePosition);

            RaycastHit hit;

            if (Physics.Raycast(rayito, out hit))
            {


                print("PEGÓ! " + hit.point + ", " + hit.transform.name + ", " + hit.transform.tag);
            }
            else {

                print("NO PEGÓ :( ");
            }
        }
    }
}
