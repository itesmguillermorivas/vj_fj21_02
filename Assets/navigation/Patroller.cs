using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Patroller : MonoBehaviour
{

    public NavNode[] path;
    public NavNode start, end;
    public float speed = 10;
    public float threshold = 0.02f;
    public float waitTime = 0.3f;

    private int currentTarget;
    

    // Start is called before the first frame update
    void Start()
    {

        currentTarget = 0;
        List<NavNode> pathList = Pathfinding.AStar(start, end);
        if (pathList != null) {

            foreach (NavNode current in pathList) {
                print(current.transform.name);
            }
        }

        path = pathList.ToArray();

        StartCoroutine(PathChanger());
    }

    // Update is called once per frame
    void Update()
    {
        // lookat rotates the object in such a way that the forward vector points
        // at the objective when done
        transform.LookAt(path[currentTarget].transform);
        transform.Translate(transform.forward * Time.deltaTime * speed, Space.World);
    }

    private IEnumerator PathChanger() {

        while (true) {

            // THIS IS SUPER UNLIKELY TO HAPPEN
            // if(path[currentTarget].transform.position == transform.position)

            // test distance
            float distance = Vector3.Distance(transform.position, path[currentTarget].transform.position);

            // note - square distance is faster
            // Vector3 betweenPositions = path[currentTarget].transform.position - transform.position;
            // float distanceAlt = betweenPositions.sqrMagnitude;

            // we are close enough
            if (distance < threshold) {

                currentTarget++;

                // to do a loop
                currentTarget %= path.Length;

                transform.LookAt(path[currentTarget].transform);
            }

            yield return new WaitForSeconds(waitTime);

        }
    }
}
